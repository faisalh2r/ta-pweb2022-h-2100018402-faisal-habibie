<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="style-ta.css">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Proses</title>
</head>
<body>
    <script language="javascript" src="validasi.js"></script>
    <header>
        <div class="logo">
            <img src="gambar/logo.png" alt="" height="60" width="60">
            Ini Portofolio
        </div>     
        <nav>
            <ul>
                <li><a href="tugas-akhir.html">Home</a></li>
                <li><a href="tugas-akhir.html">About</a></li>
                <li><a href="tugas-akhir.html">Portofolio</a></li>
                <li><a href="tugas-akhir.html">Photo</a></li>
                <li><a href="tugas-akhir.html">Contact</a></li>
            </ul>
        </nav>    
    </header>
    <main>
        <article>
        <?php
              echo "<head><title>Pesan</title></head>"; // judul
              $fp = fopen("pesan.txt","a+"); // fopen untuk membuka file, a+ buat membaca atau menulis isi file  
              $tanggal=date("Y-m-d"); // membuat var tanggal dengan format Tahun-bulan-tanggal                     
              $nama=$_POST['nama'];   // $_POST buat memanggil data yang di inputkan        
              $age=$_POST['age'];
              $email=$_POST['email'];  
              $subject=$_POST['subject'];         
              $message=$_POST['message'];
              fputs($fp,"$tanggal|$nama|$age|$email|$subject|$message\n");  // fputs menulis ke file terbuka
              fclose($fp);                                            // fclose untuk menutup file yang terbuka

              echo "Pesan anda sudah masuk ke sistem<br>";
              echo "Terima Kasih telah mengunjungi web saya<br>";    // <br> untuk baris baru
              echo "<a href=lihat.php>Klik disini </a> untuk lihat history"; // ketika klik otomatis pindah ke lihat.php
       ?>
        </article>
    </main>
    <footer>
        Copyright © · Website By Faisal Habibie
    </footer>
</body>
</html>