<?php
$nilai = 76.25;
if ($nilai >= 80) {
    echo "Nilai anda $nilai, anda mendapatkan A";
} else if ($nilai >= 76.25) {
    echo "Nilai anda $nilai, anda mendapatkan A-";
} else if ($nilai >= 68.75) {
    echo "Nilai anda $nilai, anda mendapatkan B+";
} else if ($nilai >= 65) {
    echo "Nilai anda $nilai, anda mendapatkan B";
} else if ($nilai >= 62.50) {
    echo "Nilai anda $nilai, anda mendapatkan B-";
} else if ($nilai >= 57.50) {
    echo "Nilai anda $nilai, anda mendapatkan C+";
} else if ($nilai >= 55) {
    echo "Nilai anda $nilai, anda mendapatkan C";
} else if ($nilai >= 51.25) {
    echo "Nilai anda $nilai, anda mendapatkan C-";
} else if ($nilai >= 43.75) {
    echo "Nilai anda $nilai, anda mendapatkan D+";
} else if ($nilai >= 40) {
    echo "Nilai anda $nilai, anda mendapatkan D";
} else {
    echo "Nilai anda $nilai, anda mendapatkan E";
}
