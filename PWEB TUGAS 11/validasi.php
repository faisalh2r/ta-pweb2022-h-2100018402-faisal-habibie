<html>
<head>
    <title>Validasi</title>
</head>
<body>
    <?php
    $nameErr    = $emailErr   = $genderErr  = $websiteErr = "";
    $name       = $email      = $gender     = $comment    = $website = "";
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["name"])) {
            $nameErr = "<font color=red>* Nama harus diisi</font>";
        } else {
            $name = test_input($_POST["name"]);
            if (!preg_match("/^[a-zA-z]*$/", $name)) {
                $nameErr = "Only letters and white space allowed";
            }
        }
        if (empty($_POST["email"])) {
            $emailErr = "<font color=red>* Email harus diisi</font>";
        } else {
            $email = test_input($_POST["email"]);
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $emailErr = "Invalid email format";
            }
        }
        if (empty($_POST["website"])) {
            $website = "";
        } else {
            $website = test_input($_POST["website"]);
            if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%?=~_|]/i", $website)) {
                $websiteErr = "Invalid URL";
            }
        }
        if (empty($_POST["comment"])) {
            $comment = "";
        } else {
            $comment = test_input($_POST["comment"]);
        }
        if (empty($_POST["gender"])) {
            $genderErr = "<font color=red>* Gender harus diisi</font>";
        } else {
            $gender = test_input($_POST["gender"]);
        }
    }
    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    ?>
    <h2>PHP Validasi</h2>
    <p>
        <span class="error">
           <font color="red">* harus diisi</font>
        </span>
    </p>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" name="">
        Name: <input type="text" name="name" value="<?php echo $name; ?>">
        <span class="error">
            <?php echo $nameErr; ?>
        </span>
        <br><br>
        Email: <input type="text" name="email" value="<?php echo $email; ?>">
        <span>
            <?php echo $emailErr; ?>
        </span>
        <br><br>
        Website: <input type="text" name="website" value="<?php echo $website; ?>">
        <span>
            <?php echo $websiteErr; ?>
        </span>
        <br><br>
        Comment: <textarea name="comment" id="" cols="40" rows="5" value="<?php echo $comment; ?>"></textarea>
        <span>
            <?php echo $comment; ?>
        </span>
        <br><br>
        Gender: <input type="radio" name="gender" <?php
        if (isset($gender) && $gender == "female") echo "checked"; ?> value="female">Female
        <input type="radio" name="gender" <?php
        if (isset($gender) && $gender == "male") echo "checked"; ?> value="male">Male
        <span class="error">
            <?php echo $genderErr; ?>
        </span>
        <br><br>
        <input type="submit" name="submit" value="Submit">
    </form>
    <?php
    echo "<h2>Inputanku : </h2>";
    echo $name;
    echo "<br>";
    echo $email;
    echo "<br>";
    echo $website;
    echo "<br>";
    echo $comment;
    echo "<br>";
    echo $gender;
    ?>
</body>
</html>