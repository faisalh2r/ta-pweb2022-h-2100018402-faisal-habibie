<?php
echo "Film-film kartun yang saya sukai <br>";
$arrFilm = array("Naruto", "Spongebob", "Cars");

echo "Menampilkan isi array dengan FOR : <br><br>";
for ($i = 0; $i < count($arrFilm); $i++) {
    echo "Film kesukaan saya indeks ke-$i : " . $arrFilm[$i] . "<br>";
}

echo "<br>";
echo "Menampilkan isi array dengan FOREACH : <br>";
foreach ($arrFilm as $Film) {
    echo "Film kesukaan saya " . $Film . "<br>";
}
