<?php
echo "Daftar nilai kelas H <br>";
$arrNilai = array("Adit" => 80, "Sopo" => 75, "Jarwo" => 50, "Faisal" => 100);
echo "Menampilkan isi array assosiatif dengan foreach : <br>";
foreach ($arrNilai as $nama => $nilai) {
    echo "Nilai $nama = $nilai<br>";
}

reset($arrNilai);
echo "<br>Menampilkan isi array asosiatif dengan WHILE dan LIST : <br>";
while (list($nama, $nilai) = each($arrNilai)) {
    echo "Nilai $nama = $nilai<br>";
}
